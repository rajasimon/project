# britecore

This is sample project which demonstrate my skill about Django. The question
repo [here](https://github.com/IntuitiveWebSolutions/ProductDevelopmentProject)

## Setup

```sh
$ pipenv install && pipenv install --dev #( only if you want )
$ pipenv run python manage.py migrate
$ pipenv run python manange.py runserver
```

## Deployment

```sh
$ zappa deploy
```

### Note

I've hard-corded to redirect /dev because of the lambda zappa stages name.
