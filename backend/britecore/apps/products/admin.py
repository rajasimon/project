from django.contrib import admin

from britecore.apps.products.models import Text, Number, Date, Risk, RiskField

# Register your models here.
admin.site.register(Text)
admin.site.register(Number)
admin.site.register(Date)
admin.site.register(Risk)
admin.site.register(RiskField)
