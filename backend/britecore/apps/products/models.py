from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType


# Create your models here.
class Risk(models.Model):
    """
    In order to support the multiple property and address this class is for the
    name of the property
    """
    name = models.CharField(max_length=200)
    riskfields = models.ManyToManyField('RiskField', blank=True)

    def __str__(self):
        return "{}".format(self.name)


class RiskField(models.Model):
    """
    This properties addresses field
    """
    name = models.CharField(max_length=200, null=True, blank=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    riskfield_object = GenericForeignKey('content_type', 'object_id')

    def model_type(self):
        """
        To mention which type of content_type is in the serializer I used this
        model_type to find out. So vue js plot the correct field
        """
        if self.content_type.name == 'text':
            return 'text'
        elif self.content_type.name == 'number':
            return 'number'
        elif self.content_type.name == 'date':
            return 'date'
        else:
            return False

    def __str__(self):
        return '{}'.format(self.name)


class Text(models.Model):
    """
    Represents the Text type
    """
    text = models.CharField(max_length=200, null=True, blank=True, default=None)
    relate = GenericRelation(RiskField)

    def __str__(self):
        return "{}".format(self.text)



class Number(models.Model):
    """
    Represents the Number type
    """
    number = models.IntegerField(null=True, blank=True, default=0)
    relate = GenericRelation(RiskField)

    def __str__(self):
        return "{}".format(self.number)


class Date(models.Model):
    """
    Represent the Date type
    """
    date = models.DateField(null=True, blank=True, default=None)

    def __str__(self):
        return "{}".format(self.date)
