#pylint: disable=W0223
"""
Django RestFramework file
"""
from rest_framework import serializers

from britecore.apps.products.models import Text, Number, Date, Risk, RiskField


class TextSerializer(serializers.ModelSerializer):
    """
    Represent the text field
    """
    class Meta:
        """ Subclass of TextSerializer """
        model = Text
        fields = ['id', 'text']


class NumberSerializer(serializers.ModelSerializer):
    """
    Represent the text field
    """
    class Meta:
        """ Subclass of NumberSerializer """
        model = Number
        fields = ['id', 'number']


class DateSerializer(serializers.ModelSerializer):
    """
    Represent the text field
    """
    class Meta:
        """ Subclass of DateSerializer """
        model = Date
        fields = ['id', 'date']



class RiskfieldObjectRelatedField(serializers.RelatedField):
    """
    Class represent the manytomany relation with riskfield
    """
    def to_representation(self, value):
        if isinstance(value, Text):
            serializer = TextSerializer(value)
        elif isinstance(value, Number):
            serializer = NumberSerializer(value)
        elif isinstance(value, Date):
            serializer = DateSerializer(value)
        else:
            raise Exception('Unexpected type of tagged object')
        return serializer.data


class RiskFieldSerializer(serializers.ModelSerializer):
    """
    Class based representation for riskfield model
    """
    field_type = serializers.CharField(source='model_type')
    riskfield_object = RiskfieldObjectRelatedField(read_only=True)
    class Meta:
        """ Subclass of RiskFieldSerializer """
        model = RiskField
        fields = ['id', 'name', 'riskfield_object', 'field_type']


class RiskSerializers(serializers.ModelSerializer):
    """
    Class based representation for risk model
    """  
    riskfields = RiskFieldSerializer(many=True, read_only=True)

    class Meta:
        """ Subclass of Risk """
        model = Risk
        fields = ['id', 'name', 'riskfields']
