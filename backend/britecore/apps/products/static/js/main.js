Vue.component('text-input', {
  template: '\
    <div>\
      <div class="field">\
        <label class="label">{{ label }}</label>\
        <div class="control">\
          <input class="input" type="text" placeholder="Text input">\
        </div>\
      </div>\
    </div>\
  ',
  props: ['label'],
})

Vue.component('number-input', {
  template: '\
    <div>\
      <div class="field">\
        <label class="label">{{ label }}</label>\
        <div class="control">\
          <input class="input" type="number" placeholder="Enter number">\
        </div>\
      </div>\
    </div>\
  ',
  props: ['label'],
})

Vue.component('date-input', {
  template: '\
    <div>\
      <div class="field">\
        <label class="label">{{ label }}</label>\
        <div class="control">\
          <input class="input" type="date" placeholder="Enter number" id="flatPickerId">\
        </div>\
      </div>\
    </div>\
  ',
  props: ['label']
})


new Vue({
  delimiters: ['[[', ']]'],
  el: '#app',
  template: '#my-template',
  data: {
    risk: 10,
    loading: 0
  },
  created() {
    this.fetchData()
  },
  methods: {
    fetchData() {
      let params = (new URL(document.location)).searchParams;
      let id = params.get("id");
      fetch('/dev/risk/'+ id)
      .then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => {
        console.log(response)
        this.risk = response
      });
    }
  }
})


// https://github.com/chmln/flatpickr
const fp = flatpickr("#flatPickerId", {});
