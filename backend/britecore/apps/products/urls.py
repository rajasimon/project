from django.urls import path

from britecore.apps.products import views

app_name = 'coreapp'
urlpatterns = [
    path('', views.home, name='home'),
    path('risks', views.ListRisk.as_view(), name='risks'),
    path('risk/<int:pk>', views.GetRisk.as_view(), name='risk')
]
