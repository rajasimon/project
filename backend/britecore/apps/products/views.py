"""
Coreapp view controller
"""
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import NotFound

from britecore.apps.products.models import Risk
from britecore.apps.products.serializers import RiskSerializers


# Create your views here.
def home(request):
    risk_id = request.GET.get('id')
    if risk_id:
        risk = Risk.objects.get(id=risk_id)
        return render(request, 'pages/risk.html', {'risk': risk})
    else:
        risks = Risk.objects.all()
        return render(request, 'pages/risks.html', {'risks': risks})


class GetRisk(APIView):
    """
    View to return only risk to match the given argument
    """
    def get(self, request, pk):
        """
        Returns data for given argument
        """
        try:
            obj = Risk.objects.get(pk=pk)
            serializer = RiskSerializers(obj)
            return Response(serializer.data)
        except Exception:
            raise NotFound()

class ListRisk(APIView):
    """
    Returns all the available risks
    """
    def get(self, reqest):
        """
        Returns all the risks
        """
        risk_queryset = Risk.objects.all()
        serializer = RiskSerializers(risk_queryset, many=True)
        return Response(serializer.data)
    